package com.kreativ.toaqui;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ParseUser currentUser;
    boolean back = false;
    private LocationManager lm;
    private String friendName = "HUE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_title),0);

        currentUser = ParseUser.getCurrentUser();

        defineScreenElements();

        // Define o Lacation Manager
        lm = (LocationManager) getSystemService(getApplicationContext().LOCATION_SERVICE);

        // ---------------------- MAPA
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Location lastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastLocation == null) {
            lastLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (lastLocation == null) {
            // Add a marker in Sydney and move the camera
            LatLng pucminas = new LatLng(-19.9226275, -43.9920073);
            mMap.addMarker(new MarkerOptions().position(pucminas).title("PUC Minas"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pucminas, 16.0f));

            Toast.makeText(MainActivity.this, getString(R.string.text_loc_not_found), Toast.LENGTH_SHORT).show();
        } else {
            double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();

            // Mostra a localização no mapa
            LatLng place = new LatLng(lat, lon);
            mMap.addMarker(new MarkerOptions().position(place).title(currentUser.getUsername()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 16.0f));

            Toast.makeText(MainActivity.this, getString(R.string.text_location_update), Toast.LENGTH_SHORT).show();
        }
    }

    void defineScreenElements() {
        final TextView txtMyCode = (TextView) findViewById(R.id.txtMyCode);
        txtMyCode.setText(getString(R.string.text_my_code) + " " + currentUser.getObjectId().toString());

        Button btnImHere = (Button) findViewById(R.id.btnImHere);
        btnImHere.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        btnImHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery(getString(R.string.parse_table_loc));
                query.whereEqualTo(getString(R.string.parse_key_user_id), currentUser.getObjectId());
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> itemsList, ParseException e) {
                        if (e == null) {
                            ParseObject location = new ParseObject(getString(R.string.parse_table_loc));

                            // Verifica se não é uma atualização
                            if (itemsList.size() > 0) {
                                location = itemsList.get(0);
                            }

                            Location lastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (lastLocation == null) {
                                lastLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            }

                            if (lastLocation == null) {
                                Toast.makeText(MainActivity.this, getString(R.string.text_loc_not_found), Toast.LENGTH_SHORT).show();
                            } else {
                                double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
                                location.put(getString(R.string.parse_key_lat), lat);
                                location.put(getString(R.string.parse_key_long), lon);
                                location.put(getString(R.string.parse_key_user_id), currentUser.getObjectId());
                                location.saveInBackground();

                                // Mostra a localização no mapa
                                LatLng place = new LatLng(lat, lon);
                                mMap.addMarker(new MarkerOptions().position(place).title(currentUser.getUsername()));
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 16.0f));

                                Toast.makeText(MainActivity.this, getString(R.string.text_location_update), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.parse_query_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        final EditText etxtFriendCode = (EditText) findViewById(R.id.etxtFriendCode);

        Button btnFindFriend = (Button) findViewById(R.id.btnFindFriend);
        btnFindFriend.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        btnFindFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pega o nome do usuario
                ParseQuery<ParseUser> queryUser = ParseUser.getQuery();
                queryUser.getInBackground(etxtFriendCode.getText().toString(), new GetCallback<ParseUser>() {
                    public void done(ParseUser object, ParseException e) {
                        if (e == null) {
                            friendName = object.getUsername();
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.parse_query_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                // Pega a localizacao
                ParseQuery<ParseObject> query = ParseQuery.getQuery(getString(R.string.parse_table_loc));
                query.whereEqualTo(getString(R.string.pref_user_id), etxtFriendCode.getText().toString());
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> itemsList, ParseException e) {
                        if (e == null) {
                            if (itemsList.size() > 0) {
                                double lat = (double) itemsList.get(0).getNumber(getString(R.string.parse_key_lat));
                                double lon = (double) itemsList.get(0).getNumber(getString(R.string.parse_key_long));

                                // Mostra a localização no mapa
                                LatLng place = new LatLng(lat, lon);
                                mMap.addMarker(new MarkerOptions().position(place).title(friendName));
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 16.0f));
                            } else {
                                Toast.makeText(MainActivity.this, getString(R.string.text_loc_not_found), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.parse_query_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        Button btnLoggof = (Button) findViewById(R.id.btnLoggof);
        btnLoggof.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        btnLoggof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_title),0);
                preferences.edit().putBoolean(getString(R.string.pref_user_logged), false).commit();

                ParseUser.logOut();

                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (back) {
            System.exit(0);
        } else {
            back = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    back = false;
                }
            }, 3 * 1000);
            Toast.makeText(MainActivity.this, "Pressione novamente para fechar", Toast.LENGTH_SHORT).show();
        }

    }
}
