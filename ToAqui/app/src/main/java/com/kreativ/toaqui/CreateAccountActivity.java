package com.kreativ.toaqui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class CreateAccountActivity extends AppCompatActivity {

    private Button btnCreateAccount;
    private EditText etxtUsername, etxtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount);
        etxtUsername = (EditText) findViewById(R.id.etxtUsername);
        etxtPassword = (EditText) findViewById(R.id.etxtPassword);
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = new ParseUser();
                user.setUsername(etxtUsername.getText().toString());
                user.setPassword(etxtPassword.getText().toString());

                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_title),0);
                            preferences.edit().putBoolean(getString(R.string.pref_user_logged), true).commit();

                            Toast.makeText(CreateAccountActivity.this, "Conta criada com sucesso", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(CreateAccountActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            LinearLayout createLayout = (LinearLayout) findViewById(R.id.createLayout);
                            Snackbar.make(createLayout, getResources().getString(R.string.create_failed), Snackbar.LENGTH_SHORT).show();
                            Log.e("ParseException", e.getMessage());
                        }
                    }
                });
            }
        });

    }
}
